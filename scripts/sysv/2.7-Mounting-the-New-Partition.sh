#!/usr/bin/env bash

# 2.7. Mounting the New Partition

mkdir -pv "$LFS"
mount -v -t ext4 /dev/"$LFS_EXT4_DEV" "$LFS"
/sbin/swapon -v /dev/"$LFS_SWAP_DEV"