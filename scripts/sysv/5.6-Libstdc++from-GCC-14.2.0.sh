#!/usr/bin/env bash

# 5.6. Libstdc++ from GCC-14.2.0

sudo -u lfs tar -xvf /mnt/lfs/sources/gcc-14.2.0.tar.xz -C /mnt/lfs/sources/
sudo -u lfs mkdir -v /mnt/lfs/sources/gcc-14.2.0/build

sudo -u lfs bash -c "cd /mnt/lfs/sources/gcc-14.2.0/build && /mnt/lfs/sources/gcc-14.2.0/libstdc++-v3/configure     \
    --host=x86_64-lfs-linux-gnu                                                                                     \
    --build=$(../config.guess)                                                                                      \
    --prefix=/usr                                                                                                   \
    --disable-multilib                                                                                              \
    --disable-nls                                                                                                   \
    --disable-libstdcxx-pch                                                                                         \
    --with-gxx-include-dir=/tools/x86_64-lfs-linux-gnu/include/c++/14.2.0"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/gcc-14.2.0/build
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/gcc-14.2.0/build DESTDIR=/mnt/lfs install
sudo -u lfs rm -v /mnt/lfs/usr/lib/lib{stdc++{,exp,fs},supc++}.la