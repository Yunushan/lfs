#!/usr/bin/env bash

# 6.15. Tar-1.35

sudo -u lfs tar -xvf /mnt/lfs/sources/tar-1.35.tar.xz -C /mnt/lfs/sources/
sudo -u lfs bash -c "cd /mnt/lfs/sources/tar-1.35 && /mnt/lfs/sources/tar-1.35/configure --prefix=/usr                  \
            --host=x86_64-lfs-linux-gnu                                                                                 \
            --build=$(build-aux/config.guess)"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/tar-1.35
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/tar-1.35 DESTDIR=/mnt/lfs install