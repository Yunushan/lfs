#!/usr/bin/env bash

# 6.13. Patch-2.7.6

sudo -u lfs tar -xvf /mnt/lfs/sources/patch-2.7.6.tar.xz -C /mnt/lfs/sources/
sudo -u lfs bash -c "cd /mnt/lfs/sources/patch-2.7.6 && /mnt/lfs/sources/patch-2.7.6/configure --prefix=/usr    \
            --host=x86_64-lfs-linux-gnu                                                                         \
            --build=$(build-aux/config.guess)"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/patch-2.7.6/
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/patch-2.7.6/ DESTDIR=/mnt/lfs install