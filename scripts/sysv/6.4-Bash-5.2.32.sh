#!/usr/bin/env bash

# 6.4. Bash-5.2.32

sudo -u lfs tar -xvf /mnt/lfs/sources/bash-5.2.32.tar.gz -C /mnt/lfs/sources/

sudo -u lfs bash -c "cd /mnt/lfs/sources/bash-5.2.32 && /mnt/lfs/sources/bash-5.2.32/configure --prefix=/usr        \
            --build=$(sh support/config.guess)                                                                      \
            --host=x86_64-lfs-linux-gnu                                                                             \
            --without-bash-malloc                                                                                   \
            bash_cv_strtold_broken=no"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/bash-5.2.32
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/bash-5.2.32 DESTDIR=/mnt/lfs install
sudo -u lfs ln -sv bash /mnt/lfs/bin/sh