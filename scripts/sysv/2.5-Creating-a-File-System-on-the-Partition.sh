#!/usr/bin/env bash

# 2.5. Creating a File System on the Partition

mkfs -v -t ext4 /dev/"$LFS_EXT4_DEV"
mkswap /dev/"$LFS_SWAP_DEV"