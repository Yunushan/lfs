#!/usr/bin/env bash

# 6.9. Gawk-5.3.0

sudo -u lfs tar -xvf /mnt/lfs/sources/gawk-5.3.0.tar.xz -C /mnt/lfs/sources/
sudo -u lfs sed -i 's/extras//' Makefile.in
sudo -u lfs bash -c "cd /mnt/lfs/sources/gawk-5.3.0 && /mnt/lfs/sources/gawk-5.3.0/configure --prefix=/usr      \
            --host=x86_64-lfs-linux-gnu                                                                         \
            --build=$(build-aux/config.guess)"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/gawk-5.3.0/
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/gawk-5.3.0/ DESTDIR=/mnt/lfs install