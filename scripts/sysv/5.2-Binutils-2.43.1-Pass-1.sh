#!/usr/bin/env bash

# 5.2. Binutils-2.43.1 - Pass 1

sudo -u lfs tar -xvf /mnt/lfs/sources/binutils-2.43.1.tar.xz -C /mnt/lfs/sources/
sudo -u lfs mkdir -v /mnt/lfs/sources/binutils-2.43.1/build
sudo -u lfs bash -c "cd /mnt/lfs/sources/binutils-2.43.1/build &&   \
/mnt/lfs/sources/binutils-2.43.1/configure --prefix=/mnt/lfs/tools  \
          --with-sysroot=/mnt/lfs                                   \
          --target=x86_64-lfs-linux-gnu                             \
          --disable-nls                                             \
          --enable-gprofng=no                                       \
          --disable-werror                                          \
          --enable-new-dtags                                        \
          --enable-default-hash-style=gnu"
sudo -u lfs time make -j"$CORE" -C /mnt/lfs/sources/binutils-2.43.1/build/
sudo -u lfs time make -j"$CORE" -C /mnt/lfs/sources/binutils-2.43.1/build/ install
sudo -u lfs rm -vrf /mnt/lfs/sources/binutils-2.43.1/