#!/usr/bin/env bash

# 7.2. Changing Ownership

sudo -u root chown --from lfs -R root:root "$LFS"/{usr,lib,var,etc,bin,sbin,tools}
sudo -u root bash -c "case $(uname -m) in
  x86_64) chown --from lfs -R root:root $LFS/lib64 ;;
esac"