#!/usr/bin/env bash

# 6.12. Make-4.4.1

sudo -u lfs tar -xvf /mnt/lfs/sources/make-4.4.1.tar.gz -C /mnt/lfs/sources/
sudo -u lfs bash -c "cd /mnt/lfs/sources/make-4.4.1 && /mnt/lfs/sources/make-4.4.1/configure --prefix=/usr      \
            --without-guile                                                                                     \
            --host=x86_64-lfs-linux-gnu                                                                         \
            --build=$(build-aux/config.guess)"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/make-4.4.1/
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/make-4.4.1/ DESTDIR=/mnt/lfs install