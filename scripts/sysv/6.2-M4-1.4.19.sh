#!/usr/bin/env bash

# 6.2. M4-1.4.19

sudo -u lfs tar -xvf /mnt/lfs/sources/m4-1.4.19.tar.xz -C /mnt/lfs/sources/
sudo -u lfs bash -c "cd /mnt/lfs/sources/m4-1.4.19/ && /mnt/lfs/sources/m4-1.4.19/configure --prefix=/usr   \
            --host=x86_64-lfs-linux-gnu                                                                     \
            --build=$(build-aux/config.guess)"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/m4-1.4.19/build
sudo -u lfs make DESTDIR=/mnt/lfs install