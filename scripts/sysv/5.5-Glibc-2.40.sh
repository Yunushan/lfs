#!/usr/bin/env bash

# 5.5. Glibc-2.40

sudo -u lfs tar -xvf /mnt/lfs/sources/glibc-2.40.tar.xz -C /mnt/lfs/sources/
sudo -u lfs bash -c 'case $(uname -m) in
    i?86)   ln -sfv ld-linux.so.2 $LFS/lib/ld-lsb.so.3
    ;;
    x86_64) ln -sfv ../lib/ld-linux-x86-64.so.2 $LFS/lib64
            ln -sfv ../lib/ld-linux-x86-64.so.2 $LFS/lib64/ld-lsb-x86-64.so.3
    ;;
esac'

sudo -u lfs patch -Np1 -i /mnt/lfs/sources/glibc-2.40.tar.xz/glibc-2.40-fhs-1.patch
sudo -u lfs mkdir -v /mnt/lfs/sources/glibc-2.40/build
sudo -u lfs echo "rootsbindir=/usr/sbin" > configparms

sudo -u lfs bash -c "cd /mnt/lfs/sources/glibc-2.40/build && /mnt/lfs/sources/glibc-2.40/configure  \
      --prefix=/usr                                                                                 \
      --host=x86_64-lfs-linux-gnu                                                                   \
      --build=$(../scripts/config.guess)                                                            \
      --enable-kernel=4.19                                                                          \
      --with-headers=/mnt/lfs/usr/include                                                           \
      --disable-nscd                                                                                \
      libc_cv_slibdir=/usr/lib"

sudo -u lfs time make -j"$CORE" -C /mnt/lfs/sources/glibc-2.40/build
sudo -u lfs time make -j"$CORE" -C /mnt/lfs/sources/glibc-2.40/build DESTDIR=/mnt/lfs install
sudo -u lfs sed '/RTLDLIST=/s@/usr@@g' -i /mnt/lfs/usr/bin/ldd

sudo -u lfs echo 'int main(){}' | x86_64-lfs-linux-gnu-gcc -xc -
sudo -u lfs readelf -l /mnt/lfs/sources/glibc-2.40/a.out | grep ld-linux
sudo -u lfs rm -v /mnt/lfs/sources/glibc-2.40/a.out
sudo -u lfs rm -vrf /mnt/lfs/sources/glibc-2.40/