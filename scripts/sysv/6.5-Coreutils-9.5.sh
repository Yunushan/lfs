#!/usr/bin/env bash

# 6.5. Coreutils-9.5

sudo -u lfs tar -xvf /mnt/lfs/sources/coreutils-9.5.tar.xz -C /mnt/lfs/sources/

sudo -u lfs bash -c "cd /mnt/lfs/sources/coreutils-9.5 && /mnt/lfs/sources/configure --prefix=/usr                      \
            --host=x86_64-lfs-linux-gnu                                                                                 \
            --build=$(build-aux/config.guess)                                                                           \
            --enable-install-program=hostname                                                                           \
            --enable-no-install-program=kill,uptime"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/coreutils-9.5
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/coreutils-9.5 DESTDIR=/mnt/lfs install

sudo -u lfs mv -v /mnt/lfs/usr/bin/chroot              /mnt/lfs/usr/sbin
sudo -u lfs mkdir -pv /mnt/lfs/usr/share/man/man8
sudo -u lfs mv -v /mnt/lfs/usr/share/man/man1/chroot.1 /mnt/lfs/usr/share/man/man8/chroot.8
sudo -u lfs sed -i 's/"1"/"8"/'                    /mnt/lfs/usr/share/man/man8/chroot.8