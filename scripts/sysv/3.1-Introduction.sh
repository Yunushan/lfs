#!/usr/bin/env bash

# 3.1. Introduction
mkdir -v "$LFS"/sources
chmod -v a+wt "$LFS"/sources

wget -O "$LFS/sources/wget-list" https://www.linuxfromscratch.org/lfs/downloads/stable/wget-list
wget --input-file="$LFS/sources/wget-list" --continue --directory-prefix=$LFS/sources
chown root:root "$LFS"/sources/*