#!/usr/bin/env bash

# 6.3. Ncurses-6.5

sudo -u lfs tar -xvf /mnt/lfs/sources/ncurses-6.5.tar.gz -C /mnt/lfs/sources/
sudo -u lfs mkdir -v /mnt/lfs/sources/ncurses-6.5/build

sudo -u lfs sed -i s/mawk// configure

sudo -u lfs pushd /mnt/lfs/sources/ncurses-6.5/build
sudo -u lfs bash -c "cd /mnt/lfs/sources/ncurses-6.5/ && /mnt/lfs/sources/ncurses-6.5/configure"
sudo -u lfs make -C include
sudo -u lfs make -C progs tic
sudo -u lfs popd /mnt/lfs/sources/ncurses-6.5

sudo -u lfs bash -c "cd /mnt/lfs/sources/ncurses-6.5/build && /mnt/lfs/sources/ncurses-6.5/build/configure --prefix=/usr    \
            --host=x86_64-lfs-linux-gnu                                                                                     \
            --build=$(./config.guess)                                                                                       \
            --mandir=/usr/share/man                                                                                         \
            --with-manpage-format=normal                                                                                    \
            --with-shared                                                                                                   \
            --without-normal                                                                                                \
            --with-cxx-shared                                                                                               \
            --without-debug                                                                                                 \
            --without-ada                                                                                                   \
            --disable-stripping"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/ncurses-6.5/build
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/ncurses-6.5/build DESTDIR=/mnt/lfs TIC_PATH=$(pwd)/build/progs/tic install
sudo -u lfs ln -sv /mnts/lfs/ncurses-6.5/build/libncursesw.so /mnts/lfs/usr/lib/libncurses.so
sudo -u lfs sed -e 's/^#if.*XOPEN.*$/#if 1/' \
    -i /mnt/lfs/usr/include/curses.h