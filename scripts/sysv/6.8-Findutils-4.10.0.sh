#!/usr/bin/env bash

# 6.8. Findutils-4.10.0

sudo -u lfs tar -xvf /mnt/lfs/sources/findutils-4.10.0.tar.xz -C /mnt/lfs/sources/
sudo -u lfs bash -c "cd /mnt/lfs/sources/findutils-4.10.0 && /mnt/lfs/sources/configure --prefix=/usr                   \
            --localstatedir=/var/lib/locate                                                                             \
            --host=x86_64-lfs-linux-gnu                                                                                 \
            --build=$(build-aux/config.guess)"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/findutils-4.10.0/
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/findutils-4.10.0/ DESTDIR=/mnt/lfs install