#!/usr/bin/env bash

# 6.17. Binutils-2.43.1 - Pass 2

sudo -u lfs tar -xvf /mnt/lfs/sources/binutils-2.43.1.tar.xz -C /mnt/lfs/sources/
sudo -u lfs sed '6009s/$add_dir//' -i ltmain.sh
sudo -u lfs mkdir -v /mnt/lfs/sources/binutils-2.43.1/build
sudo -u lfs bash -c "cd /mnt/lfs/sources/binutils-2.43.1/build && /mnt/lfs/sources/binutils-2.43.1/configure    \
    --prefix=/usr                                                                                               \
    --build=$(../config.guess)                                                                                  \
    --host=x86_64-lfs-linux-gnu                                                                                 \
    --disable-nls                                                                                               \
    --enable-shared                                                                                             \
    --enable-gprofng=no                                                                                         \
    --disable-werror                                                                                            \
    --enable-64-bit-bfd                                                                                         \
    --enable-new-dtags                                                                                          \
    --enable-default-hash-style=gnu"
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/binutils-2.43.1
sudo -u lfs make -j"$CORE" -C DESTDIR=/mnt/lfs install
sudo -u lfs rm -v /mnt/lfs/usr/lib/lib{bfd,ctf,ctf-nobfd,opcodes,sframe}.{a,la}