#!/usr/bin/env bash

# 6.6. Diffutils-3.10

sudo -u lfs tar -xvf /mnt/lfs/sources/diffutils-3.10.tar.xz -C /mnt/lfs/sources/

sudo -u lfs bash -c "cd /mnt/lfs/sources/diffutils-3.10 && /mnt/lfs/sources/diffutils-3.10/configure --prefix=/usr      \
            --host=x86_64-lfs-linux-gnu                                                                                 \
            --build=$(./build-aux/config.guess)"
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/diffutils-3.10
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/diffutils-3.10 DESTDIR=/mnt/lfs install