#!/usr/bin/env bash

# 5.4. Linux-6.10.5 API Headers

sudo -u lfs tar -xvf /mnt/lfs/sources/linux-6.10.5.tar.xz -C /mnt/lfs/sources/
sudo -u lfs make -j"$CORE" mrproper -C /mnt/lfs/sources/linux-6.10.5
sudo -u lfs make -j"$CORE" headers -C /mnt/lfs/sources/linux-6.10.5
sudo -u lfs find /mnt/lfs/sources/linux-6.10.5/usr/include -type f ! -name '*.h' -delete
sudo -u lfs cp -rv /mnt/lfs/sources/linux-6.10.5/usr/include /mnt/lfs/usr
sudo -u lfs rm -vrf /mnt/lfs/sources/linux-6.10.5/