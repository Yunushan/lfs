#!/usr/bin/env bash

# 4.4. Setting Up the Environment

# Get the number of cores for parallel make flags
CORE=$(nproc)

# Write to ~/.bash_profile for the lfs user
sudo -u lfs cat > /home/lfs/.bash_profile << 'EOF'
exec env -i HOME=\$HOME TERM=\$TERM PS1='\\u:\\w\$ ' /bin/bash
EOF

# Write to ~/.bashrc for the lfs user
sudo -u lfs cat > /home/lfs/.bashrc << 'EOF'
set +h
umask 022
LFS=/mnt/lfs
LC_ALL=POSIX
LFS_TGT=\$(uname -m)-lfs-linux-gnu
PATH=/usr/bin
if [ ! -L /bin ]; then PATH=/bin:\$PATH; fi
PATH=\$LFS/tools/bin:\$PATH
CONFIG_SITE=\$LFS/usr/share/config.site
export LFS LC_ALL LFS_TGT PATH CONFIG_SITE
EOF

# Move /etc/bash.bashrc if it exists (for root user)
sudo -u root [ ! -e /etc/bash.bashrc ] || mv -v /etc/bash.bashrc /etc/bash.bashrc.NOUSE

# Export MAKEFLAGS for parallel builds
sudo -u root bash -c "export MAKEFLAGS=-j$CORE"

# Append MAKEFLAGS to ~/.bashrc for the lfs user
sudo -u lfs cat >> /home/lfs/.bashrc << 'EOF'
export MAKEFLAGS=-j$CORE
EOF

# Source the .bash_profile for the lfs user to apply changes
su - lfs << 'EOF'
source /home/lfs/.bash_profile
EOF