#!/usr/bin/env bash

# 6.7. File-5.45

sudo -u lfs tar -xvf /mnt/lfs/sources/file-5.45.tar.gz -C /mnt/lfs/sources/
sudo -u lfs mkdir -v /mnt/lfs/sources/file-5.45/build
sudo -u lfs bash -c 'cd /mnt/lfs/sources/file-5.45/ && pushd /mnt/lfs/sources/file-5.45/build'
sudo -u lfs bash -c "cd /mnt/lfs/sources/file-5.45/ && /mnt/lfs/sources/file-5.45/configure --disable-bzlib         \
               --disable-libseccomp                                                                                 \
               --disable-xzlib                                                                                      \
               --disable-zlib"
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/file-5.45/build
sudo -u lfs bash -c 'cd /mnt/lfs/sources/file-5.45/build && popd /mnt/lfs/sources/file-5.45'
sudo -u lfs bash -c "cd /mnt/lfs/sources/file-5.45 && /mnt/lfs/sources/file-5.45/configure --prefix=/usr --host=x86_64-lfs-linux-gnu --build=$(./config.guess)"
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/file-5.45 FILE_COMPILE=$(pwd)/build/src/file
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/file-5.45 DESTDIR=/mnt/lfs install
sudo -u lfs rm -v /mnt/lfs/usr/lib/libmagic.la