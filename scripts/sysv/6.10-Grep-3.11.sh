#!/usr/bin/env bash

# 6.10. Grep-3.11

sudo -u lfs tar -xvf /mnt/lfs/sources/grep-3.11.tar.xz -C /mnt/lfs/sources/
sudo -u lfs bash -c "cd /mnt/lfs/sources/grep-3.11 && /mnt/lfs/sources/configure --prefix=/usr      \
            --host=x86_64-lfs-linux-gnu                                                             \
            --build=$(./build-aux/config.guess)"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/grep-3.11/
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/grep-3.11/ DESTDIR=/mnt/lfs install