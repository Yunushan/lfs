#!/usr/bin/env bash

# 6.18. GCC-14.2.0 - Pass 2

sudo -u lfs tar -xvf /mnt/lfs/sources/gcc-14.2.0.tar.xz -C /mnt/lfs/sources/
sudo -u lfs tar -xvf /mnt/lfs/sources/mpfr-4.2.1.tar.xz -C /mnt/lfs/sources/gcc-14.2.0
sudo -u lfs mv -v /mnt/lfs/sources/gcc-14.2.0/mpfr-4.2.1 /mnt/lfs/sources/gcc-14.2.0/mpfr
sudo -u lfs tar -xvf /mnt/lfs/sources/gmp-6.3.0.tar.xz -C /mnt/lfs/sources/gcc-14.2.0
sudo -u lfs mv -v /mnt/lfs/sources/gcc-14.2.0/gmp-6.3.0 /mnt/lfs/sources/gcc-14.2.0/gmp
sudo -u lfs tar -xvf /mnt/lfs/sources/mpc-1.3.1.tar.gz -C /mnt/lfs/sources/gcc-14.2.0
sudo -u lfs mv -v /mnt/lfs/sources/gcc-14.2.0/mpc-1.3.1 /mnt/lfs/sources/gcc-14.2.0/mpc
sudo -u lfs bash -c "case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
  ;;
esac"
sudo -u lfs sed '/thread_header =/s/@.*@/gthr-posix.h/' \
    -i libgcc/Makefile.in libstdc++-v3/include/Makefile.in
sudo -u lfs mkdir -v /mnt/lfs/sources/gcc-14.2.0/build
sudo -u lfs bash -c "cd /mnt/lfs/sources/gcc-14.2.0/build && /mnt/lfs/sources/gcc-14.2.0/configure      \
    --build=$(../config.guess)                                                                          \
    --host=x86_64-lfs-linux-gnu                                                                         \
    --target=x86_64-lfs-linux-gnu                                                                       \
    LDFLAGS_FOR_TARGET=-L$PWD/x86_64-lfs-linux-gnu /libgcc                                              \
    --prefix=/usr                                                                                       \
    --with-build-sysroot=/mnt/lfs                                                                       \
    --enable-default-pie                                                                                \
    --enable-default-ssp                                                                                \
    --disable-nls                                                                                       \
    --disable-multilib                                                                                  \
    --disable-libatomic                                                                                 \
    --disable-libgomp                                                                                   \
    --disable-libquadmath                                                                               \
    --disable-libsanitizer                                                                              \
    --disable-libssp                                                                                    \
    --disable-libvtv                                                                                    \
    --enable-languages=c,c++"
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/gcc-14.2.0/build
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/gcc-14.2.0/build DESTDIR=/mnt/lfs install
sudo -u lfs ln -sv gcc /mnt/lfs/usr/bin/cc