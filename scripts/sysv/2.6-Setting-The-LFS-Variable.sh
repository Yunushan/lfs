#!/usr/bin/env bash

# 2.6. Setting The $LFS Variable

export LFS=/mnt/lfs
sudo -u root bash -c "export LFS=/mnt/lfs"
echo $LFS # LFS Output Check