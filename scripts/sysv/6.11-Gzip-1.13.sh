#!/usr/bin/env bash

# 6.11. Gzip-1.13

sudo -u lfs tar -xvf /mnt/lfs/sources/gzip-1.13.tar.xz -C /mnt/lfs/sources/
sudo -u lfs bash -c "cd /mnt/lfs/sources/gzip-1.13 && /mnt/lfs/sources/gzip-1.13/configure --prefix=/usr --host=x86_64-lfs-linux-gnu"
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/gzip-1.13/
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/gzip-1.13/ DESTDIR=/mnt/lfs install