#!/usr/bin/env bash

# 7.3. Preparing Virtual Kernel File Systems

sudo -u root mkdir -pv /mnt/lfs/{dev,proc,sys,run}
sudo -u root mount -v --bind /dev /mnt/lfs/dev
mount -vt devpts devpts -o gid=5,mode=0620 $LFS/dev/pts
mount -vt proc proc $LFS/proc
mount -vt sysfs sysfs $LFS/sys
mount -vt tmpfs tmpfs $LFS/run
if [ -h $LFS/dev/shm ]; then
  install -v -d -m 1777 $LFS$(realpath /dev/shm)
else
  mount -vt tmpfs -o nosuid,nodev tmpfs $LFS/dev/shm
fi