#!/usr/bin/env bash

# 6.16. Xz-5.6.2

sudo -u lfs tar -xvf /mnt/lfs/sources/xz-5.6.2.tar.xz -C /mnt/lfs/sources/
sudo -u lfs bash -c "cd /mnt/lfs/sources/xz-5.6.2 && /mnt/lfs/sources/configure --prefix=/usr           \
            --host=x86_64-lfs-linux-gnu                                                                 \
            --build=$(build-aux/config.guess)                                                           \
            --disable-static                                                                            \
            --docdir=/usr/share/doc/xz-5.6.2"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/xz-5.6.2
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/xz-5.6.2 DESTDIR=/mnt/lfs install
sudo -u lfs rm -v /mnt/lfs/usr/lib/liblzma.la