#!/usr/bin/env bash

# 5.3-GCC-14.2.0-Pass-1.sh

sudo -u lfs tar -xvf /mnt/lfs/sources/gcc-14.2.0.tar.xz -C /mnt/lfs/sources/
sudo -u lfs tar -xvf /mnt/lfs/sources/mpfr-4.2.1.tar.xz -C /mnt/lfs/sources/gcc-14.2.0/
sudo -u lfs mv -v /mnt/lfs/sources/gcc-14.2.0/mpfr-4.2.1/ /mnt/lfs/sources/gcc-14.2.0/mpfr
sudo -u lfs tar -xvf /mnt/lfs/sources/gmp-6.3.0.tar.xz -C /mnt/lfs/sources/gcc-14.2.0/
sudo -u lfs mv -v /mnt/lfs/sources/gcc-14.2.0/gmp-6.3.0/ /mnt/lfs/sources/gcc-14.2.0/gmp
sudo -u lfs tar -xvf /mnt/lfs/sources/mpc-1.3.1.tar.gz -C /mnt/lfs/sources/gcc-14.2.0/
sudo -u lfs mv -v /mnt/lfs/sources/gcc-14.2.0/mpc-1.3.1/ /mnt/lfs/sources/gcc-14.2.0/mpc


sudo -u lfs bash -c "case \$(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig /mnt/lfs/sources/gcc-14.2.0/gcc/config/i386/t-linux64
 ;;
esac"


sudo -u lfs mkdir -v /mnt/lfs/sources/gcc-14.2.0/build
sudo -u lfs bash -c "cd /mnt/lfs/sources/gcc-14.2.0/build && /mnt/lfs/sources/gcc-14.2.0/configure  \
    --target=x86_64-lfs-linux-gnu                                                                   \
    --prefix=/mnt/lfs/tools                                                                         \
    --with-glibc-version=2.40                                                                       \
    --with-sysroot=/mnt/lfs                                                                         \
    --with-newlib                                                                                   \
    --without-headers                                                                               \
    --enable-default-pie                                                                            \
    --enable-default-ssp                                                                            \
    --disable-nls                                                                                   \
    --disable-shared                                                                                \
    --disable-multilib                                                                              \
    --disable-threads                                                                               \
    --disable-libatomic                                                                             \
    --disable-libgomp                                                                               \
    --disable-libquadmath                                                                           \
    --disable-libssp                                                                                \
    --disable-libvtv                                                                                \
    --disable-libstdcxx                                                                             \
    --enable-languages=c,c++"

sudo -u lfs chmod -R u+x /mnt/lfs/sources/gcc-14.2.0/build
sudo -u lfs time make -j"$CORE" -C /mnt/lfs/sources/gcc-14.2.0/build
sudo -u lfs time make -j"$CORE" -C /mnt/lfs/sources/gcc-14.2.0/build install

sudo -u lfs cat /mnt/lfs/sources/gcc-14.2.0/gcc/limitx.h /mnt/lfs/sources/gcc-14.2.0/gcc/glimits.h /mnt/lfs/sources/gcc-14.2.0/gcc/limity.h > \
  `dirname $(x86_64-lfs-linux-gnu-gcc -print-libgcc-file-name)`/include/limits.h
sudo -u lfs rm -vrf /mnt/lfs/sources/gcc-14.2.0/