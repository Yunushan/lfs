#!/usr/bin/env bash

# 6.14. Sed-4.9

sudo -u lfs tar -xvf /mnt/lfs/sources/sed-4.9.tar.xz -C /mnt/lfs/sources/
sudo -u lfs bash -c "cd /mnt/lfs/sources/sed-4.9 && /mnt/lfs/sources/sed-4.9/configure --prefix=/usr    \
            --host=x86_64-lfs-linux-gnu                                                                 \
            --build=$(./build-aux/config.guess)"

sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/sed-4.9/
sudo -u lfs make -j"$CORE" -C /mnt/lfs/sources/sed-4.9/ DESTDIR=/mnt/lfs install