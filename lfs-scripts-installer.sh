#!/usr/bin/env bash

# Loading Bar

printf "Installation Starting"
value=0
while [ $value -lt 600 ];do
    value=$((value+20))
    printf "."
    sleep 0.05
done
printf "\n"

CPUARCH=$(uname -m)
CORE=$(nproc)
SCRIPTS_PATH=$(find / -name "scripts" | grep -i "lfs/scripts" | head -n 1)
ALL_LVS=$(lvdisplay | grep -i "lv path" | awk '{print $3}' | awk '{print substr($0, 6)}')
ALL_LVS_SIZE=$(lvdisplay | grep -i "lv size" | awk '{print $3,$4}' | awk '{print substr($0, 2)}')
ALL_DEV=$(fdisk -l | grep -i /dev/ | grep -iv "Disk /dev/" | awk '{print $1}' | awk '{print substr($0, 6)}')
# Beginnig of  UI Input

# LFS Installation Selection
printf "\nPlease Choose Your Desired LFS Version\n\n1-)Sysv (12.2)\n2-)Sysd (12.2)\n3-)Docker Sysv (12.2)\n4-)Docker Sysd (12.2)\n\nPlease Enter Your LFS Installation Selection(1-4):"
while true;do
    read -r LFS_INSTALLATION_SELECTION
    if [[ $LFS_INSTALLATION_SELECTION = "1" ]] || [[ $LFS_INSTALLATION_SELECTION = "2" ]] || [[ $LFS_INSTALLATION_SELECTION = "3" ]] ||\
        [[ $LFS_INSTALLATION_SELECTION = "4" ]];then
        printf "Chosen option $LFS_INSTALLATION_SELECTION\n"
        break
    else
        printf "Please Input Between (1-4):"
    fi
done

if [ "$LFS_INSTALLATION_SELECTION" = "1" ];then
    # Beginning of SYSV Installation
    # Loading Bar

    printf "Installation Starting Please Wait"
    value=0
    while [ $value -lt 600 ];do
        value=$((value+20))
        printf "."
        sleep 0.05
    done
    printf "\n"


    LFS_STABLE_SYSV=$(lynx -dump https://www.linuxfromscratch.org/lfs/downloads/stable/ | awk '/http/{print $2}' | grep -i sysv-nochunks | head -n 1)
    LFS_STABLE_SYSV_VERSION_CHECK_OUTPUT=$(lynx -dump "$LFS_STABLE_SYSV" | sed -n -e'/version-check.sh/,/bash version-check.sh/ p'| sed '$d')
    LFS_STABLE_SYSV=$(cat $LFS_STABLE_SYSV)
    wget $LFS_STABLE_SYSV --continue --directory-prefix=$SCRIPTS_PATH/sysv
    # Create Downloads folder and version-check.sh file inside of that
    sudo mkdir -p /root/Downloads/lfs
    echo "$LFS_STABLE_SYSV_VERSION_CHECK_OUTPUT" > /root/Downloads/lfs/version-check.sh
    fdisk -l
    printf "\nAll Devices\n\n$ALL_DEV\n"
    printf "\n\nPlease Enter Your LFS Root Partition:" # For 1-Creating-a-File-System-on-the-Partition.sh
    read -r LFS_EXT4_DEV
    printf "Please Enter Your LFS Swap Partition:" # # For 1-Creating-a-File-System-on-the-Partition.sh
    read -r LFS_SWAP_DEV
    printf "Please Enter Your LFS Password:" # 6-Adding-the-LFS-User.sh
    read -rs LFS_PASSWORD
    . "$SCRIPTS_PATH/sysv/2.5-Creating-a-File-System-on-the-Partition.sh"
    . "$SCRIPTS_PATH/sysv/2.6-Setting-The-LFS-Variable.sh"
    . "$SCRIPTS_PATH/sysv/2.7-Mounting-the-New-Partition.sh"
    . "$SCRIPTS_PATH/sysv/3.1-Introduction.sh"
    . "$SCRIPTS_PATH/sysv/4.2-Creating-a-Limited-Directory-Layout-in-the-LFS-Filesystem.sh"
    . "$SCRIPTS_PATH/sysv/4.3-Adding-the-LFS-User.sh"
    . "$SCRIPTS_PATH/sysv/4.4-Setting-Up-the-Environment.sh"
    . "$SCRIPTS_PATH/sysv/5.2-Binutils-2.43.1-Pass-1.sh"
    . "$SCRIPTS_PATH/sysv/5.3-GCC-14.2.0-Pass-1.sh"
    . "$SCRIPTS_PATH/sysv/5.4-Linux-6.10.5-API-Headers.sh"
    . "$SCRIPTS_PATH/sysv/5.5-Glibc-2.40.sh"
    . "$SCRIPTS_PATH/sysv/5.6-Libstdc++from-GCC-14.2.0.sh"
    . "$SCRIPTS_PATH/sysv/6.2-M4-1.4.19.sh"
    . "$SCRIPTS_PATH/sysv/6.3-Ncurses-6.5.sh"
    . "$SCRIPTS_PATH/sysv/6.4-Bash-5.2.32.sh"
    . "$SCRIPTS_PATH/sysv/6.5-Coreutils-9.5.sh"
    . "$SCRIPTS_PATH/sysv/6.6-Diffutils-3.10.sh"
    . "$SCRIPTS_PATH/sysv/6.7-File-5.45.sh"
    . "$SCRIPTS_PATH/sysv/6.8-Findutils-4.10.0.sh"
    . "$SCRIPTS_PATH/sysv/6.9-Gawk-5.3.0.sh"
    . "$SCRIPTS_PATH/sysv/6.10-Grep-3.11.sh"
    . "$SCRIPTS_PATH/sysv/6.11-Gzip-1.13.sh"
    . "$SCRIPTS_PATH/sysv/6.12-Make-4.4.1.sh"
    . "$SCRIPTS_PATH/sysv/6.13-Patch-2.7.6.sh"
    . "$SCRIPTS_PATH/sysv/6.14-Sed-4.9.sh"
    . "$SCRIPTS_PATH/sysv/6.15-Tar-1.35.sh"
    . "$SCRIPTS_PATH/sysv/6.16-Xz-5.6.2.sh"
    . "$SCRIPTS_PATH/sysv/6.17-Binutils-2.43.1-Pass-2.sh"
    . "$SCRIPTS_PATH/sysv/6.18-GCC-14.2.0-Pass-2.sh"
    . "$SCRIPTS_PATH/sysv/7.2-Changing-Ownership.sh"
    . "$SCRIPTS_PATH/sysv/7.3-Preparing-Virtual-Kernel-File-Systems.sh"
    . "$SCRIPTS_PATH/sysv/7.4-Entering-the-Chroot-Environment.sh"
elif [ "$LFS_INSTALLATION_SELECTION" = "2" ];then
    printf "sysd"
    printf "Installation Starting"
    value=0
    while [ $value -lt 600 ];do
        value=$((value+20))
        printf "."
        sleep 0.05
    done
    printf "\n"
    LFS_STABLE_SYSD=$(lynx -dump https://www.linuxfromscratch.org/lfs/downloads/stable-systemd | awk '/http/{print $2}' | grep -i sysd-nochunks | head -n 1)
    LFS_STABLE_SYSD_VERSION_CHECK_OUTPUT=$(lynx -dump "$LFS_STABLE_SYSD" | sed -n -e'/version-check.sh/,/bash version-check.sh/ p'| sed '$d')
elif [ "$LFS_INSTALLATION_SELECTION" = "3" ];then
    printf "Docker sysv"
    printf "Installation starting"
    value=0
    while [ $value -lt 600 ];do
        value=$((value+20))
        printf "."
        sleep 0.05
    done
    printf "\n"
elif [ "$LFS_INSTALLATION_SELECTION" = "4" ];then
    printf "Docker sysd"
    printf "Installation starting"
    value=0
    while [ $value -lt 600 ];do
        value=$((value+20))
        printf "."
        sleep 0.05
    done
    printf "\n"
fi